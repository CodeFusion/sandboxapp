using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Support.V7.App;
using Android.Graphics;
using Android.Webkit;
using System.Threading;
using Android.Support.V7.Widget;
using Android.Widget;
using Android.Support.Design.Widget;
using Android.Support.V4.App;

namespace SandboxApp.Droid
{
    // Manifest Data
    [Activity(Label = "NewsDetailActivity", ParentActivity = typeof(MainActivity))]
    [MetaData ("android.support.PARENT_ACTIVITY", Value = ".MainActivity")]
    public class NewsDetailActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.NewsDetail);
            int newsId = Intent.GetIntExtra("newsId", -1);

            // Set up the toolbar
            Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.Title = GetString(Resource.String.app_name);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            //If we have an EventId
            if (newsId != -1)
            {
                // Go online for data in another thread
                ThreadPool.QueueUserWorkItem(o => getNews(newsId));
                /* This should eventually be sent in from the previous activity to save data.
                 * We already get all the data from the list call, it should be stored,
                 * probably in an SQLite database, and retreived by ID from here
                 */
            }
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.eventDetail, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home: // Action Bar back button
                    // Go to parent activity
                    NavUtils.NavigateUpFromSameTask(this);
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }

        private void getNews(int newsId)
        {
            ApiClient client = new ApiClient();

            News detailNews;

            // Get event data or show error
            try
            {
                detailNews = client.getArticle(newsId);

            }
            catch (Exception)
            {
                showConnectionError();
                return;
            }

            // Put data in UI
            RunOnUiThread(() => populateView(detailNews));
        }

        private void populateView(News n)
        {
            // Find Views
            ImageView iv = (ImageView)FindViewById(Resource.Id.backdrop);
            TextView textDate = (TextView)FindViewById(Resource.Id.newsDate);
            TextView textAuthor = (TextView)FindViewById(Resource.Id.newsAuthor);
            WebView wv = (WebView)FindViewById(Resource.Id.newsContent);
            CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout)FindViewById(Resource.Id.collapsing_toolbar);

            // Turn image byte array into an actual image
            if (n.image != "")
            {
                Bitmap image = BitmapFactory.DecodeByteArray(n.imageData, 0, n.imageData.Length);
                iv.SetImageBitmap(image);
            }

            // Put the data in the fields
            collapsingToolbar.SetTitle(n.title);
            textDate.Text = n.publish_at.ToString("D");
            textAuthor.Text = "By " + n.author;
            wv.SetBackgroundColor(Color.Transparent);
            wv.LoadData(n.content, "text/html", "UTF-8");
        }

        public void showConnectionError()
        {
            // Not implemented
        }
    }
}