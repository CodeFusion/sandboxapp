﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.OS;
using System.Collections.Generic;
using System.Threading;
using BottomNavigationBar;
using Android.Support.V7.App;
using Android.Support.V4.Content;
using Android.Support.Design.Widget;
using Android.Support.V7.Widget;
using Android.Graphics.Drawables;
using Android.Graphics;
using Android.Preferences;
using Android.Support.V4.Widget;

namespace SandboxApp.Droid
{

    // Manifest data goes here rather than in AndroidManifest.xml
	[Activity (Name = "ca.codefusion.sandboxes.MainActivity", Label = "NS Sandboxes", MainLauncher = true, Icon = "@drawable/icon", Theme ="@style/Sandbox", LaunchMode = Android.Content.PM.LaunchMode.SingleTop)]
	public class MainActivity : AppCompatActivity, BottomNavigationBar.Listeners.IOnTabClickListener
	{        
		private List<Event> eventItems = new List<Event> ();
        private RecyclerView recyclerView;
        private RecyclerView.LayoutManager layoutManager;
        private BottomBar _bottomBar;
        private DrawerLayout drawerLayout;
        private ISharedPreferences prefs;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

            //get user preferences
            prefs = PreferenceManager.GetDefaultSharedPreferences(this);

            //if user has selected the alternative menu
            if (prefs.GetBoolean("pref_alt_menu", false))
            {
                // Set our view from the "MainHamburger" layout resource
                SetContentView(Resource.Layout.MainHamburger);
                drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);

                // Set up the toolbar
                Toolbar toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
                SetSupportActionBar(toolbar);
                SupportActionBar.Title = GetString(Resource.String.app_name);

                //Attach listeners to hamburger menu
                var navView = FindViewById<NavigationView>(Resource.Id.nav_view);
                navView.NavigationItemSelected += NavigationView_NavigationItemSelected;
                navView.Menu.GetItem(1).SetChecked(true);

                var drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, Resource.String.open_drawer, Resource.String.close_drawer);
                drawerLayout.AddDrawerListener(drawerToggle);
                drawerToggle.SyncState();
                
            }
            else //use normal bottom menu
            {
                // Set our view from the "main" layout resource
                SetContentView(Resource.Layout.Main);

                // Set up the toolbar
                Toolbar toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
                SetSupportActionBar(toolbar);
                SupportActionBar.Title = GetString(Resource.String.app_name);

                // Set up the bottom bar items
                BottomBarTab newsTab = new BottomBarTab(Resource.Drawable.newspaper, "News");
                BottomBarTab eventsTab = new BottomBarTab(Resource.Drawable.calendar, "Events");
                BottomBarTab registeredTab = new BottomBarTab(Resource.Drawable.calendar_check, "Registered");
                BottomBarTab[] tabs = new BottomBarTab[] { newsTab, eventsTab, registeredTab };

                // Create the bottom bar and make it shy
                var coord = FindViewById<CoordinatorLayout>(Resource.Id.coordinator);
                _bottomBar = BottomBar.AttachShy(coord, FindViewById(Resource.Id.listView1), bundle);
                _bottomBar.SetItems(tabs);
                _bottomBar.SetActiveTabColor(new Android.Graphics.Color(ContextCompat.GetColor(this, Resource.Color.md_cyan_700)));
                _bottomBar.SetDefaultTabPosition(1);
                _bottomBar.SetOnTabClickListener(this);
            }

            // Set up the RecyclerView
            recyclerView = FindViewById<RecyclerView>(Resource.Id.listView1);
            layoutManager = new LinearLayoutManager(this);
            recyclerView.SetLayoutManager(layoutManager);
            recyclerView.AddItemDecoration(new SimpleDividerItemDecoration(this));

            // Go online for data in another thread
            ThreadPool.QueueUserWorkItem(o => populateEventsList());

        }

        protected override void OnResume()
        {
            base.OnResume();

            // If the user has changed the view setting
            if((prefs.GetBoolean("pref_alt_menu", false) && drawerLayout == null) || (!prefs.GetBoolean("pref_alt_menu", false) && _bottomBar == null))
            {
                //restart the activity so the view resets
                Finish();
                StartActivity(Intent);
            }
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            // choose the right Action Bar menu
            if (prefs.GetBoolean("pref_alt_menu", false))
            {
                MenuInflater.Inflate(Resource.Menu.mainActivityHamburger, menu);
            }
            else
            {
                MenuInflater.Inflate(Resource.Menu.mainActivity, menu);
            }
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.appSettings:
                    // Start the preferences activity
                    var prefsActivity = new Intent(this, typeof(PrefsActivity));
                    StartActivity(prefsActivity);
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }

        private void populateEventsList()
        {
            ApiClient client = new ApiClient();

            recyclerView = FindViewById<RecyclerView>(Resource.Id.listView1);
            List<Event> events;

            // Get the events list or show error
            try
            {
                events = new List<Event>(client.getEvents(1));
            }catch(Exception)
            {
                RunOnUiThread(() => showConnectionError());
                return;
            }

            // Create a new adapter and display it in the UI
            EventAdapter eventAdapter = new EventAdapter(events);
            eventAdapter.ItemClick += OnEventClick;
            RunOnUiThread(() => recyclerView.SetAdapter(eventAdapter));
        }

        private void OnEventClick(object sender, int position)
        {
            // Get the event that was clicked
            EventAdapter ea = (EventAdapter) recyclerView.GetAdapter();
            Event e = ea.GetEventAt(position);

            // Start the eventDetailActivity and send the EventId
            var eventDetailActivity = new Intent(this, typeof(EventDetailActivity));
            eventDetailActivity.PutExtra("eventId", e.id);
            StartActivity(eventDetailActivity);
        }

        private void populateNewsList()
        {
            ApiClient client = new ApiClient();

            recyclerView = FindViewById<RecyclerView>(Resource.Id.listView1);
            List<News> news;

            // Get the news list or show error
            try
            {
                news = new List<News>(client.getNews(1));
            }
            catch (Exception)
            {
                RunOnUiThread(() => showConnectionError());
                return;
            }

            // Create a new adapter and display it in the UI
            NewsAdapter newsAdapter = new NewsAdapter(news);
            newsAdapter.ItemClick += OnNewsClick;
            RunOnUiThread(() => recyclerView.SetAdapter(newsAdapter));
        }

        private void OnNewsClick(object sender, int position)
        {
            // Get the event that was clicked
            NewsAdapter na = (NewsAdapter)recyclerView.GetAdapter();
            News n = na.GetNewsAt(position);

            // Start the eventDetailActivity and send the EventId
            var newsDetailActivity = new Intent(this, typeof(NewsDetailActivity));
            newsDetailActivity.PutExtra("newsId", n.id);
            StartActivity(newsDetailActivity);
        }

        protected override void OnSaveInstanceState(Bundle outState)
        {
            base.OnSaveInstanceState(outState);

            //if there's a bottom bar, save its state
            if (_bottomBar != null)
            {
                _bottomBar.OnSaveInstanceState(outState);
            }
        }

        private void showConnectionError()
        {
            // Show connection error
            // Not Implemented
        }

        public void OnTabReSelected(int position)
        {
            /* According to Material Design docs,
             * reselecting a tab should return the
             * user to the top of the list
             */
        }

        public void OnTabSelected(int position)
        {
            switch (position)
            { 
                case 0: // News Tab
                    ThreadPool.QueueUserWorkItem(o => populateNewsList());
                    break;
                case 1: //Events Tab
                    ThreadPool.QueueUserWorkItem(o => populateEventsList());
                    break;
            }
        }

        private void NavigationView_NavigationItemSelected(object sender, NavigationView.NavigationItemSelectedEventArgs e)
        {
            switch (e.MenuItem.ItemId)
            {
                case Resource.Id.nav_news:
                    drawerLayout.CloseDrawers();
                    ThreadPool.QueueUserWorkItem(o => populateNewsList());
                    break;
                case Resource.Id.nav_events:
                    drawerLayout.CloseDrawers();
                    ThreadPool.QueueUserWorkItem(o => populateEventsList());
                    break;
                case Resource.Id.nav_settings:
                    drawerLayout.CloseDrawers();

                    // Start preferences activity
                    var prefsActivity = new Intent(this, typeof(PrefsActivity));
                    StartActivity(prefsActivity);
                    break;
            }
        }

        /*
         * This class adds a line between items in the recycler view
         */
        public class SimpleDividerItemDecoration : RecyclerView.ItemDecoration
        {
            private Drawable mDivider;

            public SimpleDividerItemDecoration(Context context)
            {
                mDivider = ContextCompat.GetDrawable(context, Resource.Drawable.line_divider);
            }

            public override void OnDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state)
            {
                int left = parent.PaddingLeft;
                int right = parent.Width - parent.PaddingRight;

                int childCount = parent.ChildCount;
                for (int i = 0; i < childCount; i++)
                {
                    View child = parent.GetChildAt(i);

                    RecyclerView.LayoutParams mParams = (RecyclerView.LayoutParams)child.LayoutParameters;

                    int top = child.Bottom + mParams.BottomMargin;
                    int bottom = top + mDivider.IntrinsicHeight;

                    mDivider.SetBounds(left, top, right, bottom);
                    mDivider.Draw(c);
                }
            }
        }
    }
}


