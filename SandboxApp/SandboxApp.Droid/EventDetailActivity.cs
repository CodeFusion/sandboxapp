using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Support.V7.App;
using Android.Graphics;
using Android.Webkit;
using System.Threading;
using Android.Support.V7.Widget;
using Android.Widget;
using Android.Support.Design.Widget;
using Android.Support.V4.App;

namespace SandboxApp.Droid
{
    // Manifest Data
    [Activity(Label = "EventDetailActivity", ParentActivity = typeof(MainActivity))]
    [MetaData ("android.support.PARENT_ACTIVITY", Value = ".MainActivity")]
    public class EventDetailActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.EventDetail);
            int eventId = Intent.GetIntExtra("eventId", -1);

            // Set up the toolbar
            Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.Title = GetString(Resource.String.app_name);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            //If we have an EventId
            if (eventId != -1)
            {
                // Go online for data in another thread
                ThreadPool.QueueUserWorkItem(o => getEvent(eventId));
                /* This should eventually be sent in from the previous activity to save data.
                 * We already get all the data from the list call, it should be stored,
                 * probably in an SQLite database, and retreived by ID from here
                 */
            }
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.eventDetail, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home: // Action Bar back button
                    // Go to parent activity
                    NavUtils.NavigateUpFromSameTask(this);
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }

        private void getEvent(int eventId)
        {
            ApiClient client = new ApiClient();

            Event detailEvent;

            // Get event data or show error
            try
            {
                detailEvent = client.getEvent(eventId);

            }
            catch (Exception)
            {
                showConnectionError();
                return;
            }

            // Put data in UI
            RunOnUiThread(() => populateView(detailEvent));
        }

        private void populateView(Event e)
        {
            // Find Views
            ImageView iv = (ImageView)FindViewById(Resource.Id.backdrop);
            TextView textDate = (TextView)FindViewById(Resource.Id.eventDate);
            TextView textLocation = (TextView)FindViewById(Resource.Id.eventLocation);
            WebView wv = (WebView)FindViewById(Resource.Id.eventContent);
            CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout)FindViewById(Resource.Id.collapsing_toolbar);

            // Format the date properly
            string eventDate;
            if (e.start_time.DayOfYear == e.end_time.DayOfYear)
            {
                eventDate = e.start_time.ToString("f") + " - " + e.end_time.ToString("t");
            }
            else
            {
                eventDate = e.start_time.ToString("f") + " - " + e.end_time.ToString("f");
            }

            // Turn image byte array into an actual image
            if (e.image != "")
            {
                Bitmap image = BitmapFactory.DecodeByteArray(e.imageData, 0, e.imageData.Length);
                iv.SetImageBitmap(image);
            }

            // Put the data in the fields
            collapsingToolbar.SetTitle(e.title);
            textDate.Text = eventDate;
            textLocation.Text = e.location;
            wv.SetBackgroundColor(Color.Transparent);
            wv.LoadData(e.content, "text/html", "UTF-8");
        }

        public void showConnectionError()
        {
            // Not implemented
        }
    }
}