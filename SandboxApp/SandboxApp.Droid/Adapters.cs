using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Support.V7.Widget;
using Android.Widget;

namespace SandboxApp.Droid
{
    public class EventViewHolder : RecyclerView.ViewHolder
    {
        public TextView eventTitle { get; private set; }
        public TextView eventSnippet { get; private set; }
        public TextView eventDayName { get; private set; }
        public TextView eventDate { get; private set; }

        public EventViewHolder (View itemView, Action<int> listener) : base(itemView)
        {
            eventTitle = itemView.FindViewById<TextView> (Resource.Id.eventTitle);
            eventSnippet = itemView.FindViewById <TextView> (Resource.Id.eventSnippet);
            eventDayName = itemView.FindViewById <TextView> (Resource.Id.eventDayName);
            eventDate = itemView.FindViewById <TextView> (Resource.Id.eventDate);

            itemView.Click += (sender, e) => listener(AdapterPosition);
        }
    }


    public class EventAdapter : RecyclerView.Adapter
    {
        public List<Event> mEvents;

        public EventAdapter(List<Event> events)
        {
            mEvents = events;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.EventListItem, parent, false);
            EventViewHolder vh = new EventViewHolder(itemView, OnClick);
            return vh;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            EventViewHolder vh = holder as EventViewHolder;

            vh.eventTitle.Text = mEvents[position].title;
            vh.eventSnippet.Text = mEvents[position].content;
            vh.eventDayName.Text = mEvents[position].start_time.ToString("ddd");
            vh.eventDate.Text = mEvents[position].start_time.Day.ToString();
        }

        public Event GetEventAt(int position)
        {
            return mEvents[position];
        }

        public event EventHandler<int> ItemClick;

        public override int ItemCount
        {
            get { return mEvents.Count;  }
        }

        void OnClick(int position)
        {
            if (ItemClick != null)
                ItemClick(this, position);
        }
    }


    public class NewsViewHolder : RecyclerView.ViewHolder
    {
        public TextView newsTitle { get; private set; }
        public TextView newsSnippet { get; private set; }
        public TextView newsDate { get; private set; }

        public NewsViewHolder(View itemView, Action<int> listener) : base(itemView)
        {
            newsTitle = itemView.FindViewById<TextView>(Resource.Id.newsTitle);
            newsSnippet = itemView.FindViewById<TextView>(Resource.Id.newsSnippet);
            newsDate = itemView.FindViewById<TextView>(Resource.Id.newsDate);

            itemView.Click += (sender, e) => listener(AdapterPosition);
        }
    }


    public class NewsAdapter : RecyclerView.Adapter
    {
        public List<News> mNews;

        public NewsAdapter(List<News> news)
        {
            mNews = news;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.NewsListItem, parent, false);
            NewsViewHolder vh = new NewsViewHolder(itemView, OnClick);
            return vh;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            NewsViewHolder vh = holder as NewsViewHolder;

            vh.newsTitle.Text = mNews[position].title;
            vh.newsSnippet.Text = mNews[position].content;
            vh.newsDate.Text = mNews[position].publish_at.ToString("MMMM d, yyyy");
        }

        public event EventHandler<int> ItemClick;

        public News GetNewsAt(int position)
        {
            return mNews[position];
        }

        public override int ItemCount
        {
            get { return mNews.Count; }
        }

        void OnClick(int position)
        {
            if (ItemClick != null)
                ItemClick(this, position);
        }
    }
}