using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Preferences;
using Android.Support.V7.App;
using Android.Support.Design.Widget;

namespace SandboxApp.Droid
{
    // Manifest data
    [Activity(Label = "PrefsActivity", ParentActivity = typeof(MainActivity), Theme = "@style/Sandbox.ActionBar")]
    [MetaData("android.support.PARENT_ACTIVITY", Value = ".MainActivity")]
    public class PrefsActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set title and enable back button
            SupportActionBar.Title = "Preferences";
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            // Load the settings fragment
            FragmentManager.BeginTransaction().Replace(Android.Resource.Id.Content, new PrefFragment()).Commit();

        }

        // PreferenceFragment is the replacement for PreferenceActivity
        public class PrefFragment : PreferenceFragment
        {

            public override void OnCreate(Bundle savedInstanceState)
            {
                base.OnCreate(savedInstanceState);

                AddPreferencesFromResource(Resource.Xml.preferences);
            }

        }
    }
}