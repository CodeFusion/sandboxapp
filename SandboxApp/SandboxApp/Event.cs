﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SandboxApp
{
    public class Event
    {
        public int id { get; set; }
        public string title { get; set; }
        public string author { get; set; }
        public string snippet { get; set; }
        public string content { get; set; }
        public string url { get; set; }
        public string location { get; set; }
        public string image { get; set; }
        public byte[] imageData { get; set; }
        public DateTime start_time { get; set; }
        public DateTime end_time { get; set; }

        public Event(string title, string author, string content, string url, DateTime startTime, DateTime endTime)
        {
            this.title = title;
            this.author = author;
            this.content = content;
            this.url = url;
            this.start_time = startTime;
            this.end_time = endTime;
        }

        public override string ToString()
        {
            return this.title;
        }
    }

}
