﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace SandboxApp
{
    public class ApiClient
    {

        private string baseUri = "http://sandbox.laboratory.cf/api";
        private WebClient client;

        // Create a new API Client.
        public ApiClient()
        {
            client = new WebClient();
            client.Headers.Add("user-agent", "NS Sandboxes Client");
        }

        // Get a list of events
        /// <param name="page">Indicates the page to return (optional).</param>
        /// <returns>Returns a List of Event objects</returns>
        public IList<Event> getEvents(int page = 1)
        {
            string response;

            if (page == 1)
            {
                response = client.DownloadString(new Uri(baseUri + "/events"));
            }
            else if(page > 1){
                response = client.DownloadString(new Uri(baseUri + "/events?page=" + page));
            }
            else
            {
                throw new ArgumentOutOfRangeException();
            }
            JObject result = JObject.Parse(response);
            IList<Event> events = result["events"].ToObject<List<Event>>();
            return events;
        }

        public Event getEvent(int event_id)
        {
            string response;
            if(event_id >= 0)
            {
                response = client.DownloadString(new Uri(baseUri + "/events/" + event_id));
            }
            else
            {
                throw new ArgumentOutOfRangeException();
            }
            Event singleEvent = JsonConvert.DeserializeObject<Event>(response);
            if (singleEvent.image != "")
            {
                singleEvent.imageData = client.DownloadData(new Uri(singleEvent.image));
            }

            return singleEvent;
        }

        public IList<News> getNews(int page = 1)
        {
            string response;

            if (page == 1)
            {
                response = client.DownloadString(new Uri(baseUri + "/news"));
            }
            else if (page > 1)
            {
                response = client.DownloadString(new Uri(baseUri + "/news?page=" + page));
            }
            else
            {
                throw new ArgumentOutOfRangeException();
            }
            JObject result = JObject.Parse(response);
            IList<News> news = result["news"].ToObject<IList<News>>();
            return news;
        }

        public News getArticle(int article_id)
        {
            string response;
            if (article_id >= 0)
            {
                response = client.DownloadString(new Uri(baseUri + "/news/" + article_id));
            }
            else
            {
                throw new ArgumentOutOfRangeException();
            }
            News news = JsonConvert.DeserializeObject<News>(response);
            if (news.image != "")
            {
                news.imageData = client.DownloadData(new Uri(news.image));
            }

            return news;
        }

    }
}
