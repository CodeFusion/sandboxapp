﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SandboxApp
{
    class Sandbox
    {
        public string name { get; set; }
        public string address { get; set; }
        public string email { get; set; }
        public string url { get; set; }

        public Sandbox(string name, string address, string email, string url)
        {
            this.name = name;
            this.address = address;
            this.email = email;
            this.url = url;
        }
    }
}
