﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SandboxApp
{
    public class News
    {
        public int id { get; set; }
        public string title { get; set; }
        public string author { get; set; }
        public string content { get; set; }
        public string url { get; set; }
        public string image { get; set; }
        public byte[] imageData { get; set; }
        public DateTime publish_at { get; set; }

        public News(string title, string author, string content, string url, DateTime publish_at)
        {
            this.title = title;
            this.author = author;
            this.content = content;
            this.url = url;
            this.publish_at = publish_at;
        }
    }
}
